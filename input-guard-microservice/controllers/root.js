import { checkIfFileExists, checkIfValidCSV } from '../utils/root.js';
import axios from 'axios';

const calculate = async (req, res) => {
  const { file, product } = req.body;

  // Case 1: If the file name is not provided
  if (!file) {
    return res.status(200).json({ file: null, error: 'Invalid JSON input.' });
  }

  // Case 2: If a filename is provided, but not found in the mounted disk volume
  if (file) {
    const { fileExists } = checkIfFileExists({ file });
    if (!fileExists) {
      return res.status(200).json({ file, error: 'File not found.' });
    }
  }

  // Case 3: If a filename is provided, but the file contents cannot be parsed due to not following the CSVformat
  try {
    const isValid = await checkIfValidCSV({ file });
    if (!isValid) {
      res.status(200).json({ file, error: 'Input file not in CSV format.' });
      return;
    }
  } catch (error) {
    res.status(200).json({ file, error: 'Input file not in CSV format.' });
    return;
  }

  try {
    const response = await axios.post(
      'http://data-calc-microservice:7001/sum',
      {
        file,
        product
      }
    );

    res.json(response.data);
  } catch (error) {
    if (error.response && error.response.data) {
      res.status(error.response.status).json(error.response.data);
    } else {
      console.log('error', error);
      res.status(500).json({ error: 'Internal server error.' });
    }
  }
};

export { calculate };
