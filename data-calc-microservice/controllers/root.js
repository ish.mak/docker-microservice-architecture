import { readCsv, parseCsv, calculateTotalAmount } from '../utils/root.js';

const sum = async (req, res) => {
  try {
    const { file, product } = req.body;

    // 1. read csv
    const csvData = readCsv({ file });

    // 2. parse csv data
    const parsedData = parseCsv({ csvData });

    // 3. calculate amount
    const amount = calculateTotalAmount(parsedData, product);

    res.status(200).json({ file, sum: amount });
    return;
  } catch (error) {
    if (error.response && error.response.data) {
      res.status(error.response.status).json(error.response.data);
    } else {
      res.status(500).json({ error: 'Internal server error.' });
    }
  }
};

export { sum };
