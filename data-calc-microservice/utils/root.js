import fs from 'fs';
import { VOLUME_PATH } from '../constants/index.js';

const readCsv = ({ file }) => {
  const filePath = `${VOLUME_PATH}/${file}`;

  const data = fs.readFileSync(filePath, 'utf8');
  return data;
};

const parseCsv = ({ csvData }) => {
  // Split the CSV data into individual lines
  const lines = csvData.split('\n');

  const header = lines[0].split(',');

  // Initialize an empty array to store the data
  const data = [];

  for (let i = 1; i < lines.length; i++) {
    // Split the line into values
    const values = lines[i].split(',');

    // Create an object to store the data for the current line
    const rowData = {};

    // Iterate over the values and assign them to the corresponding header keys
    for (let j = 0; j < header.length; j++) {
      rowData[header[j]] = values[j];
    }

    data.push(rowData);
  }

  return data;
};

const calculateTotalAmount = (data, product) => {
  let totalAmount = 0;

  for (let i = 0; i < data.length; i++) {
    if (data[i].product === product) {
      totalAmount += parseInt(data[i].amount);
    }
  }

  return totalAmount;
};

export { readCsv, parseCsv, calculateTotalAmount };
