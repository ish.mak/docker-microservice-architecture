## Objective

The objective is to build a microservice architecture consisting of two simple web application containers. These containers will communicate with each other through a Docker network, enabling them to provide more complex functionality by leveraging the microservices approach.

## System

![Architecture](./readme/architecture.png)
